import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './components/App';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// const elemet = document.createElement('h1');
// elemet.innerText = "Welcome to react";
// const container = document.getElementById('root');
// container.appendChild(elemet);